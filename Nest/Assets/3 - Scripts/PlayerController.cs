﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float maxSpeed;
    public float rotateSpeed;
    public ControllerHome controllerHome;
    public GameObject starSystem;
    private Color color;
    public ParticleSystem particleSys;
    public Image title;

	// Use this for initialization
	void Start () {
        color = new Color(0, 0, 0);
        particleSys.Stop();
	}

    private void Update()
    {
        //Debug.Log((17.5 - Vector3.Distance(starSystem.transform.position, this.transform.position))/10.3);
        if(((17.5 - Vector3.Distance(starSystem.transform.position, this.transform.position)) / 10.3) < 0f)
            controllerHome.SetDistanceVolumeHome(0f);
        else if (((17.5 - Vector3.Distance(starSystem.transform.position, this.transform.position)) / 10.3) > 1f)
            controllerHome.SetDistanceVolumeHome(1f);
        else
            controllerHome.SetDistanceVolumeHome((17.5f - Vector3.Distance(starSystem.transform.position, this.transform.position)) / 10.3f);
        if (this.transform.position.x + 6 < starSystem.transform.position.x)
            controllerHome.SetStereoHome(1f);
        else if (this.transform.position.x - 6 > starSystem.transform.position.x)
            controllerHome.SetStereoHome(-1f);
        else
            controllerHome.SetStereoHome((starSystem.transform.position.x - this.transform.position.x) / 6);
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetAxis("Vertical-trigger") != 0)
        {
            this.GetComponent<Rigidbody2D>().AddForce(this.transform.up * Input.GetAxis("Vertical-trigger") * speed);
            if (Input.GetAxis("Vertical-trigger") > 0)
                this.GetComponent<Animator>().SetBool("Accelerate", true);
            else
                this.GetComponent<Animator>().SetBool("Accelerate", false);
            if (color.r < 1f)
            {
                color = new Color(color.r + 0.01f, color.g + 0.01f, color.b + 0.01f);
                title.color = new Color(1, 1, 1, title.color.a - 0.02f);
                this.GetComponent<SpriteRenderer>().color = color;
                if (particleSys.isStopped)
                    particleSys.Play();
            }
            else
                particleSys.Stop();

        }
        else
        {
            this.GetComponent<Animator>().SetBool("Accelerate", false);
            particleSys.Stop();
        }
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, DirectionWithJoystick());
        }
        if (this.GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
            this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;

    }

    //Fonction gèrant l'orientation du viseur au joystick
    float DirectionWithJoystick()
    {
        //Prendre les 2 Axis du second Pad du joystick. Calculer l'angle formé par le joystick et l'appliquer à la rotation du gun
        float axis2Horizontal = Input.GetAxis("Horizontal");
        float axis2Vertical = Input.GetAxis("Vertical");
        return AngleBetweenVector3(new Vector3(0, 0, 0), new Vector3(axis2Horizontal, axis2Vertical, 0));
        //joystickRotation = Quaternion.Euler(0, 0, angle);
    }

    //Permet de calculer l'angle entre 2 vector3 dans un espace 2D
    private float AngleBetweenVector3(Vector3 vec1, Vector3 vec2)
    {
        Vector3 difference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return (Vector3.Angle(Vector3.right, difference) * sign) - 90;
    }

}
