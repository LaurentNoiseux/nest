﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
    public GameObject panelPause;
    private bool paused;
	// Use this for initialization
	void Start () {
        panelPause.SetActive(false);
        paused = false;
	}


	public void PauseEvent()
    {
        panelPause.SetActive(false);
        paused = false;
        Time.timeScale = 1.0f;
    }
    public void ResetEvent()
    {
        panelPause.SetActive(false);
        paused = false;
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Master Scene");
    }

    public void QuitEvent()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Start"))
        {
            if (!paused)
            {
                panelPause.SetActive(true);
                paused = true;
                Time.timeScale = 0f;
            }
            else
            {
                panelPause.SetActive(false);
                paused = false;
                Time.timeScale = 1.0f;
            }
        }
    }
}
