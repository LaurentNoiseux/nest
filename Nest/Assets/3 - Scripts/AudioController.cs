﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

    public List<AudioSource> audioList;
    public List<AudioClip> clipList;
    public List<Image> imageList;
    public Sprite upArrow, downArrow, leftArrow, rightArrow;
    private List<Planete> planeteList;
    public List<int> sequence;
    public GameObject panelSequence;
    private bool record;
    private bool resetDpad;
    public ControllerHome controllerHome;
    public GameObject vaisseau;
    public GameObject echolocalPrefab;
    public bool isFiring;
    public bool melodyEnable;
   
	void Start () {

        panelSequence.SetActive(false);
        sequence = new List<int>();
        planeteList = controllerHome.GetListePlanete();
    }
	
	// Update is called once per frame
	void Update ()
    {

		if(Input.GetButtonDown("Fire3") && melodyEnable)
        {

            if(!record)
            {

                Debug.Log("record");
                panelSequence.SetActive(true);
                record = true;
                resetDpad = false;
                ResetSequence();
            }
            else
            {
                Debug.Log("stop record");
                panelSequence.SetActive(false);         
                record = false;
            }

        }

        else if (Input.GetButtonDown("Fire2") && !isFiring)
        {
            planeteList = controllerHome.GetListePlanete();
            isFiring = true;
            StartCoroutine(PlaySound());
        }

        if (record)
        {
            if (!resetDpad)
            {
                if (sequence.Count < 8)
                {
                    if (Input.GetAxis("Vertical3") < 0)
                    {
                        Debug.Log(2);
                        AddtoSequence(2);
                        resetDpad = true;
                    }
                    else if (Input.GetAxis("Vertical3") > 0)
                    {
                        Debug.Log(4);
                        AddtoSequence(4);
                        resetDpad = true;
                    }
                    else if (Input.GetAxis("Horizontal3") < 0)
                    {
                        Debug.Log(3);
                        AddtoSequence(3);
                        resetDpad = true;
                    }
                    else if (Input.GetAxis("Horizontal3") > 0)
                    {
                        Debug.Log(1);
                        AddtoSequence(1);
                        resetDpad = true;
                    }
                }
            }
            else
                if (Input.GetAxis("Vertical3") == 0 && Input.GetAxis("Horizontal3") == 0)
                    resetDpad = false;
        }
        
    }
    private void AddtoSequence(int newSound)
    {
        sequence.Add(newSound);
        switch (newSound)
        {
            case 1:
                //Image right
                imageList[sequence.Count - 1].sprite = rightArrow;
                imageList[sequence.Count - 1].rectTransform.localPosition = new Vector3(imageList[sequence.Count - 1].rectTransform.localPosition.x, -100, imageList[sequence.Count - 1].rectTransform.localPosition.z);
                imageList[sequence.Count - 1].color = new Color(1, 1, 1, 1);
                break;
            case 2:
                //Image Up
                imageList[sequence.Count - 1].sprite = upArrow;
                imageList[sequence.Count - 1].rectTransform.localPosition = new Vector3(imageList[sequence.Count - 1].rectTransform.localPosition.x, 350, imageList[sequence.Count - 1].rectTransform.localPosition.z);
                imageList[sequence.Count - 1].color = new Color(1, 1, 1, 1);
                break;
            case 3:
                //Image left
                imageList[sequence.Count-1].sprite = leftArrow;
                imageList[sequence.Count - 1].rectTransform.localPosition = new Vector3(imageList[sequence.Count - 1].rectTransform.localPosition.x, 120, imageList[sequence.Count - 1].rectTransform.localPosition.z);
                imageList[sequence.Count - 1].color = new Color(1, 1, 1, 1);
                break;
            case 4:
                //Image down
                imageList[sequence.Count - 1].sprite = downArrow;
                imageList[sequence.Count - 1].rectTransform.localPosition = new Vector3(imageList[sequence.Count - 1].rectTransform.localPosition.x, -330, imageList[sequence.Count - 1].rectTransform.localPosition.z);
                imageList[sequence.Count - 1].color = new Color(1, 1, 1, 1);
                break;
                
        }        
    }
    private void ResetSequence()
    {
        foreach(Image i in imageList)
        {
            i.color = new Color(1, 1, 1, 0);
        }
        sequence.Clear();
    }

    private IEnumerator PlaySound()
    {
        for (int i = 0; i < sequence.Count; i++)
        {
            int j = 0;
            foreach(Planete planet in planeteList)
            {
                planet.PlaySound(sequence[i], audioList[j]);
                j++;
            }
            GameObject echolocal = Instantiate(echolocalPrefab, vaisseau.transform);
            if(i < sequence.Count-1)
                echolocal.GetComponent<Echolocalisation>().trigger = false;
            else
                echolocal.GetComponent<Echolocalisation>().trigger = true;
            yield return new WaitForSeconds(1f);
        }
        isFiring = false;
        
    }
}
//switch (tone)
//{
//    case 1:
//        clipList[1]
//        break;
//    case 2:
//        for (int i = 0; i < 3; i++)
//        {

//        }
//        break;
//    case 3:
//        for (int i = 0; i < 3; i++)
//        {

//        }
//        break;
//    case 4:
//        for (int i = 0; i < 3; i++)
//        {

//        }
//        break;
//    }
