﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappinTrigger : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Planet"))
        {
            this.GetComponentInParent<GrappinController>().OnChildrenTriggerEnter(collision);
        }
    }
}
