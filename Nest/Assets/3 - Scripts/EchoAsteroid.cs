﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchoAsteroid : MonoBehaviour {

    public AudioSource audioSource;
    public Planete planet;
    public int soundNumber;
    public Color color;
    private bool isFiring;
    public float time;
    public float scale;
    //public GameObject vaisseau;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(echolocal(0));
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator echolocal(float wait)
    {
        while (wait < time)
        {
            wait += Time.deltaTime;
            this.transform.localScale = new Vector3(scale * (wait / time), scale * (wait / time), 0);
            this.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 1.5f - (1.5f * (wait / time)));
            yield return null;
        }
        this.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0);
        this.GetComponent<CircleCollider2D>().enabled = false;
        Destroy(this.gameObject);
    }

}
