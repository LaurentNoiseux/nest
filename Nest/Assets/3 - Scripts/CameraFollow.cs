﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject vaisseau;
    public GameObject star;
    public float distanceFromStar;
    public GameObject wallLeft, wallUp, wallRight, wallDown;
    public float margeVertical, margeHorizontal;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(vaisseau.transform.position.x < (wallRight.transform.position.x - margeHorizontal) && 
            vaisseau.transform.position.x > (wallLeft.transform.position.x + margeHorizontal))
        {
            if (vaisseau.transform.position.y < (wallUp.transform.position.y - margeVertical) &&
            vaisseau.transform.position.y > (wallDown.transform.position.y + margeVertical))
            {
                this.transform.position = new Vector3(vaisseau.transform.position.x, vaisseau.transform.position.y, this.transform.position.z);
            }
            else
            {
                this.transform.position = new Vector3(vaisseau.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }
        else
        {
            if (vaisseau.transform.position.y < (wallUp.transform.position.y - margeVertical) &&
            vaisseau.transform.position.y > (wallDown.transform.position.y + margeVertical))
            {
                this.transform.position = new Vector3(this.transform.position.x, vaisseau.transform.position.y, this.transform.position.z);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }



        if (Vector3.Distance(vaisseau.transform.position, star.transform.position) < distanceFromStar)
        {
            this.GetComponent<Camera>().orthographicSize = 10 - 5 * (Vector3.Distance(vaisseau.transform.position, star.transform.position) / distanceFromStar);
            if (this.GetComponent<Camera>().orthographicSize > 8)
                this.GetComponent<Camera>().orthographicSize = 8;
        }

    }
}
