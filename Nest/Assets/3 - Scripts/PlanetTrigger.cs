﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetTrigger : MonoBehaviour {

    public bool hooked;
    public bool inSystem;
    public bool asteroid;
    public char namePlanet;
    public AudioController audioController;
    public GameObject echoResponsePrefab;
    public ControllerHome controllerHome;
    private Planete planet;
    public Color colorPlanet;
    public int numberInSystem;
    public GameObject vaisseau;

	// Use this for initialization
	void Start () {
        planet = new Planete();
        vaisseau = GameObject.FindGameObjectWithTag("Vaisseau");
        planet.LoadPlanet(namePlanet,audioController.clipList,1.0f);
        numberInSystem = 0;
        if (asteroid)
        {
            inSystem = true;
            StartCoroutine(AsteroidNoise(0));
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (((17.5 - Vector3.Distance(vaisseau.transform.position, this.transform.position)) / 12.5) < 0f)
            planet.SetVolume(0f);
        else if (((17.5 - Vector3.Distance(vaisseau.transform.position, this.transform.position)) / 12.5) > 1f)
            planet.SetVolume(1f);
        else
            planet.SetVolume((17.5f - Vector3.Distance(vaisseau.transform.position, this.transform.position)) / 12.5f);
        if (vaisseau.transform.position.x + 4 < this.transform.position.x)
            planet.SetStereoPan(1f);
        else if (vaisseau.transform.position.x - 4 > this.transform.position.x)
            planet.SetStereoPan(- 1f);
        else
            planet.SetStereoPan(( this.transform.position.x - vaisseau.transform.position.x) /4);

        if(asteroid && !inSystem)
        {
            this.GetComponent<CircleCollider2D>().enabled = false;
            this.GetComponent<Rigidbody2D>().AddForce((this.GetComponent<Rigidbody2D>().velocity.normalized) * 200);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Star") && !hooked && !asteroid)
        {
            if (numberInSystem <= 3)
            {
                if (Vector2.SqrMagnitude(this.GetComponent<Rigidbody2D>().velocity + Vector2.Perpendicular(this.transform.position - collision.gameObject.transform.position)) >
                    Vector2.SqrMagnitude(this.GetComponent<Rigidbody2D>().velocity - Vector2.Perpendicular(this.transform.position - collision.gameObject.transform.position)))
                    this.GetComponent<Rigidbody2D>().AddForce(Vector2.Perpendicular(this.transform.position - collision.gameObject.transform.position) * 20);
                else
                    this.GetComponent<Rigidbody2D>().AddForce(-Vector2.Perpendicular(this.transform.position - collision.gameObject.transform.position) * 20);
                this.GetComponent<DistanceJoint2D>().enabled = true;
                this.GetComponent<DistanceJoint2D>().enableCollision = true;
                this.GetComponent<DistanceJoint2D>().connectedBody = collision.GetComponent<Rigidbody2D>();
                if (!inSystem)
                {
                    if ((8.7f - Vector3.Distance(this.transform.position, collision.gameObject.transform.position)) / 5.7f < 0f)
                        controllerHome.AddStar(namePlanet,0f);
                    else if ((8.7f - Vector3.Distance(this.transform.position, collision.gameObject.transform.position)) / 5.7f > 1f)
                        controllerHome.AddStar(namePlanet,1f);
                    else
                        controllerHome.AddStar(namePlanet, (8.7f - Vector3.Distance(this.transform.position, collision.gameObject.transform.position)) / 5.7f);
                    collision.gameObject.GetComponent<StarSystem>().removeCircle();
                    collision.gameObject.GetComponent<StarSystem>().numberOfPlanet++; 
                }
                inSystem = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce((this.transform.position - collision.gameObject.transform.position) * 50);
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Star"))
        {
            numberInSystem = collision.gameObject.GetComponent<StarSystem>().numberOfPlanet+1;
        }

        if (collision.gameObject.CompareTag("Klaxon"))
        {
            StartCoroutine(Response(0));
        }
    }


    IEnumerator Response(float wait)
    {
        int tempo = 0;
        while(wait < audioController.sequence.Count)
        {
            if(wait > tempo)
            {
                GameObject echoResponse = Instantiate(echoResponsePrefab, this.transform.position, new Quaternion(0,0,0,0));
                echoResponse.GetComponent<EchoResponse>().planet = planet;
                echoResponse.GetComponent<EchoResponse>().audioSource = this.GetComponent<AudioSource>();
                echoResponse.GetComponent<EchoResponse>().soundNumber = audioController.sequence[tempo];
                echoResponse.GetComponent<EchoResponse>().color = colorPlanet;
                tempo++;
            }
            wait += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator AsteroidNoise(float wait)
    {
        int tempo = 0;
        while (inSystem)
        {
            if (wait > tempo)
            {
                GameObject echoResponse = Instantiate(echoResponsePrefab, this.transform.position, new Quaternion(0, 0, 0, 0));
                echoResponse.GetComponent<EchoAsteroid>().color = colorPlanet;
                tempo++;
            }
            wait += Time.deltaTime;
            yield return null;
        }
        controllerHome.RemoveBeurk();
        audioController.melodyEnable = true;
    }
}
