﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planete
{
    private char name;
    private List<AudioClip> listClipsPlanete;
    private float volume;
    private float volumeModifier, volumeModifierHome;
    private float stereoPan, stereoPanHome;

    private bool planetActive;

    
    public Planete()
    {
        listClipsPlanete = new List<AudioClip>();
        planetActive = false;
    }
    public Planete(AudioClip newClip1, AudioClip newClip2, AudioClip newClip3, AudioClip newClip4, AudioSource newAudioSource)
    {
        listClipsPlanete = new List<AudioClip>
        {
            newClip1,
            newClip2,
            newClip3,
            newClip4
        };
        planetActive = true;
    }


    public void PlaySound(int tone, AudioSource audio)
    {
        if (planetActive)
        {
            audio.volume = volume*volumeModifier;
            audio.panStereo = stereoPan;
            audio.clip = listClipsPlanete[tone - 1];
            audio.Play();
        }
    }
    public void PlaySoundHome(int tone, AudioSource audio)
    {
        if (planetActive)
        {
            audio.volume = volume * volumeModifierHome;
            audio.panStereo = stereoPanHome;
            audio.clip = listClipsPlanete[tone - 1];
            audio.Play();
        }
    }

    public void LoadPlanet(char newName, List<AudioClip> clipList, float newVolume)
    {
        switch (newName)
        {
            case 'A':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[0],
                    clipList[1],
                    clipList[2],
                    clipList[3],
                };
                break;
            case 'B':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[4],
                    clipList[5],
                    clipList[6],
                    clipList[7],
                };
                break;
            case 'C':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[8],
                    clipList[9],
                    clipList[10],
                    clipList[11],
                };
                break;
            case 'D':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[12],
                    clipList[13],
                    clipList[14],
                    clipList[15],
                };
                break;
            case 'E':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[16],
                    clipList[17],
                    clipList[18],
                    clipList[19],
                };
                break;
            case 'F':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[20],
                    clipList[21],
                    clipList[22],
                    clipList[23],
                };
                break;
            case 'G':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[24],
                    clipList[25],
                    clipList[26],
                    clipList[27],
                };
                break;
            case 'H':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[28],
                    clipList[29],
                    clipList[30],
                    clipList[31],
                };
                break;
            case 'I':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[32],
                    clipList[33],
                    clipList[34],
                    clipList[35],
                };
                break;
            case 'J':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[36],
                    clipList[37],
                    clipList[38],
                    clipList[39],
                };
                break;
            case 'Z':
                listClipsPlanete = new List<AudioClip>
                {
                    clipList[40],
                    clipList[41],
                    clipList[42],
                    clipList[43],
                };
                break;
        }
        planetActive = true;
        name = newName;
        volume = newVolume;
        volumeModifier = 1f;
        volumeModifierHome = 1f;
        stereoPan = 0f;
        stereoPanHome = 1f;
    }

    public void clearPlanet()
    {
        planetActive = false;
    }
    public char GetName()
    {
        return name;
    }
    public void SetVolume(float newVolumeModifier)
    {
        volumeModifier = newVolumeModifier;
    }
    public void SetStereoPan(float newStereoPan)
    {
        stereoPan = newStereoPan;
    }
    public void SetVolumeHome(float newVolumeModifier)
    {
        volumeModifierHome = newVolumeModifier;
    }
    public void SetStereoPanHome(float newStereoPan)
    {
        stereoPanHome = newStereoPan;
    }
}