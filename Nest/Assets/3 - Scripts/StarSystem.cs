﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystem : MonoBehaviour {

    public GameObject cerclePointille;
    public int numberOfPlanet;

	// Use this for initialization
	void Start () {
        numberOfPlanet = 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Planet") && numberOfPlanet < 3)
        {
            if(collision.gameObject.GetComponent<PlanetTrigger>().hooked && !collision.gameObject.GetComponent<PlanetTrigger>().asteroid)
            {
                float distance = Vector3.Distance(this.transform.position, collision.gameObject.transform.position);
                cerclePointille.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
                cerclePointille.transform.localScale = new Vector3(distance / 99, distance / 99, 0);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Planet"))
        {
            removeCircle();
        }
    }

    public void removeCircle()
    {
        cerclePointille.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0f);
    }
}
