﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappinController : MonoBehaviour {

    bool isFiring;
    bool hooked;
    public Rigidbody2D vaisseau;
    public float time;
    public float distance;
    GameObject planetHooked;
    public StarSystem starSystem;

	// Use this for initialization
	void Start () {
        isFiring = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1") && !isFiring)
        {
            StartCoroutine(FireHook(0));
            isFiring = true;
        }
        else if (Input.GetButtonDown("Fire1") && hooked)
        {
            hooked = false;
            isFiring = false;
            planetHooked.GetComponent<PlanetTrigger>().hooked = false;
            planetHooked.GetComponent<DistanceJoint2D>().connectedBody = null;
            planetHooked.GetComponent<DistanceJoint2D>().enabled = false;
            planetHooked.GetComponent<Rigidbody2D>().mass = 100;
            this.transform.localScale = new Vector3(1, 0, 0);
            this.transform.localRotation = Quaternion.Euler(0, 0, 0);
            this.GetComponentInChildren<BoxCollider2D>().enabled = true;
        }
    }

    IEnumerator FireHook(float wait)
    {
        while (wait < time && !hooked)
        {
            wait += Time.deltaTime;
            this.transform.localScale = new Vector3(1, distance * (wait / time),0);
            yield return null;
        }
        if(!hooked)
        {
            this.transform.localScale = new Vector3(1, 0, 0);
            isFiring = false;
        }
    }

    public void OnChildrenTriggerEnter(Collider2D collision)
    {
        hooked = true;
        this.GetComponentInChildren<BoxCollider2D>().enabled = false;
        collision.gameObject.GetComponent<PlanetTrigger>().hooked = true;
        if(collision.gameObject.GetComponent<PlanetTrigger>().inSystem)
        {
            starSystem.numberOfPlanet--;
            collision.gameObject.GetComponent<PlanetTrigger>().controllerHome.removePlanet(collision.gameObject.GetComponent<PlanetTrigger>().namePlanet);
            collision.gameObject.GetComponent<PlanetTrigger>().inSystem = false;
        }

        collision.gameObject.GetComponent<DistanceJoint2D>().enabled = true;
        collision.gameObject.GetComponent<DistanceJoint2D>().connectedBody = vaisseau;
        collision.gameObject.GetComponent<Rigidbody2D>().mass = 1;
        planetHooked = collision.gameObject;
        StartCoroutine(Hook(collision.gameObject,vaisseau.gameObject));
    }

    IEnumerator Hook(GameObject planet, GameObject ship)
    {
        while(hooked)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, AngleBetweenVector3(ship.transform.position, planet.transform.position));
            this.transform.localScale = new Vector3(1,Vector3.Distance(ship.transform.position, planet.transform.position)*5,0);
            yield return null;
        }
    }

    //Permet de calculer l'angle entre 2 vector3 dans un espace 2D
    private float AngleBetweenVector3(Vector3 vec1, Vector3 vec2)
    {
        Vector3 difference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return (Vector3.Angle(Vector3.right, difference) * sign) - 90;
    }
}
